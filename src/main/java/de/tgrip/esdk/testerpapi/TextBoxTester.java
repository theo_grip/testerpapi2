package de.tgrip.esdk.testerpapi;

import de.abas.eks.jfop.FOPException;
import de.abas.eks.jfop.remote.ContextRunnable;
import de.abas.eks.jfop.remote.FOPSessionContext;
import de.abas.erp.api.gui.ButtonSet;
import de.abas.erp.api.gui.TextBox;
import de.abas.erp.common.type.enums.EnumDialogBox;
import de.abas.erp.db.DbContext;

public class TextBoxTester implements ContextRunnable {

    @Override
    public int runFop(FOPSessionContext fopSessionContext, String[] strings) throws FOPException {
        DbContext dbContext = fopSessionContext.getDbContext();

        TextBox textBox = new TextBox(dbContext, "Info testbox", "some information");
        textBox.setButtons(ButtonSet.YES_NO);
        EnumDialogBox result = textBox.show();

        dbContext.out().println("Test box tester " + result);

        return 0;
    }
}
