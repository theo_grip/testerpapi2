package de.tgrip.esdk.testerpapi;

import de.abas.eks.jfop.FOPException;
import de.abas.eks.jfop.remote.ContextRunnable;
import de.abas.eks.jfop.remote.FOPSessionContext;
import de.abas.erp.api.gui.GUISelectionBuilder;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.Query;
import de.abas.erp.db.schema.employee.Employee;
import de.abas.erp.db.selection.Selection;

public class GuiSelectionBuilderTester implements ContextRunnable {

    @Override
    public int runFop(FOPSessionContext ctx, String[] strings) throws FOPException {
        DbContext dbContext = ctx.getDbContext();

        Selection<Employee> employeeSelection = GUISelectionBuilder.select(dbContext, Employee.class);
        dbContext.out().println("selection " + employeeSelection.getCriteria());
        Query<Employee> employeeQuery = dbContext.createQuery(employeeSelection);
        for (Employee employee : employeeQuery) {
            dbContext.out().println("Employee " + employee.getIdno() + " - " + employee.getSwd());
        }

        return 0;
    }
}
