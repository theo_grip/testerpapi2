package de.tgrip.esdk.testerpapi;

import de.abas.eks.jfop.FOPException;
import de.abas.eks.jfop.remote.ContextRunnable;
import de.abas.eks.jfop.remote.FOPSessionContext;
import de.abas.erp.api.gui.MenuBuilder;
import de.abas.erp.db.DbContext;

public class MenuBuilderTester implements ContextRunnable {

    @Override
    public int runFop(FOPSessionContext ctx, String[] strings) throws FOPException {
        DbContext dbContext = ctx.getDbContext();

        MenuBuilder<String> menuBuilder = new MenuBuilder<>(dbContext, "Choose one");
        menuBuilder.addItem("1", "eins");
        menuBuilder.addItem("2", "zwei");
        String result = menuBuilder.show();

        dbContext.out().println("Menu builder tester with " + result);

        return 0;
    }
}
