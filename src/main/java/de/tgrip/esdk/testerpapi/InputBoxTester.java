package de.tgrip.esdk.testerpapi;

import de.abas.eks.jfop.FOPException;
import de.abas.eks.jfop.remote.ContextRunnable;
import de.abas.eks.jfop.remote.FOPSessionContext;
import de.abas.erp.api.gui.InputBox;
import de.abas.erp.db.DbContext;

public class InputBoxTester implements ContextRunnable {

    @Override
    public int runFop(FOPSessionContext ctx, String[] args) throws FOPException {
        DbContext dbContext = ctx.getDbContext();

        InputBox box = new InputBox(dbContext, "Please enter a message");
        String read = box.read(true);

        dbContext.out().println("Input box tester read " + read);
        return 0;
    }
}
